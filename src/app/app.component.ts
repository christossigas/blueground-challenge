import { Component, Input , OnInit } from '@angular/core';
import { City } from './city';
import { FilterPipe } from './filter.pipe';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse} from '@angular/common/http';
import {PostComponent} from './post.component';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ FilterPipe ],
 
})

export class AppComponent {
  	title = 'Cities';

	cities = [
	new City(5, 'Detroit'),
	new City(3, 'Chicago'),
	new City(1, 'New York'),
	new City(4, 'Los Angeles'),
	new City(2, 'San Francisco')
];
}
