import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule, OnInit, Injectable } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { CityComponent } from './city/city.component';
import { FilterPipe } from './filter.pipe';
import { HttpClientModule } from '@angular/common/http';
import { NgbdModalComponent } from './modal-component';
import { NgbdModalContent } from './modal-component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

 
@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    CityComponent,
    NgbdModalComponent,
    NgbdModalContent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [
    AppComponent,
    NgbdModalContent]
})

export class AppModule { }

